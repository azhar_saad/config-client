package com.howtodoinjava.example.springconfigclient;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.howtodoinjava.example.springconfigclient.SSLClientFactory.HttpClientType;

@SpringBootApplication
public class SpringConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringConfigClientApplication.class, args);
	}

	@Autowired
	public void setEnv(Environment e) {
		System.out.println(e.getProperty("msg"));
	}

	/*
	 * for services registerd in eureka naming server
	 */
	@Bean("rest-template-eureka")
	@LoadBalanced
	public RestTemplate restTemplate(RestTemplateBuilder builder) {

		RestTemplate restTemplate = builder.setConnectTimeout(Duration.ofMinutes(1))
				.setReadTimeout(Duration.ofSeconds(30)).build();

		CloseableHttpClient httpClient = HttpClients.custom().setHostnameVerifier(new AllowAllHostnameVerifier())
				.build();

		try {
			HttpClient httpsClient = HttpClients.custom()
					.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
					.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		restTemplate.setRequestFactory(SSLClientFactory.getClientHttpRequestFactory(HttpClientType.HttpClient));
		return restTemplate;
	}

	/*
	 * non-eureka clients
	 */
	@Bean("rest-template")
	public RestTemplate restTemplateNonEureka(RestTemplateBuilder builder) {

		RestTemplate restTemplate = builder.setConnectTimeout(Duration.ofMinutes(1))
				.setReadTimeout(Duration.ofSeconds(60)).build();

		restTemplate.setRequestFactory(SSLClientFactory.getClientHttpRequestFactory(HttpClientType.HttpClient));
		return restTemplate;
	}

//	public static RestTemplate getRestTemplate()
//			throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
//		// TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String
//		// authType) -> true;
//
//		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
//
//			public boolean isTrusted(final X509Certificate[] chain, String authType) throws CertificateException {
//				// Oh, I am easy...
//				return true;
//			}
//
//		};
//
//		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
//				.build();
//
//		// SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
//
//		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new String[] { "TLSv1.2" }, null,
//				new NoopHostnameVerifier());
//
//		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
//
//		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
//		/*
//		 * HttpComponentsClientHttpRequestFactory requestFactory = new
//		 * HttpComponentsClientHttpRequestFactory( HttpClientBuilder.create()
//		 * .setProxy(new HttpHost("proxycacheST.hewitt.com", 3228, "http")) .build());
//		 */
//
//		requestFactory.setHttpClient(httpClient);
//		RestTemplate restTemplate = new RestTemplate(requestFactory);
//		return restTemplate;
//	}
}

@RefreshScope
@RestController
class MessageRestController {

	@Value("${msg:Config Server is not working. Please check.....}")
	private String msg;

	@CrossOrigin
	@GetMapping("/msg")
	public String getMsg() {
		return this.msg;
	}
}
